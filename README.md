# sae-2019-12-factor

## Initial setup
Basically you need to ensure that `./Docker/.env` exists and is filled up. In order to get the `APP_KEY` you will need to execute `php artisan key:generate` cli command which you can perform by issuing the following from the Docker subfolder. 

```
docker run -ti -v ${PWD}/../app:/app -w /app composer:latest /bin/bash -c "composer install --no-scripts --no-plugins --no-suggest; php artisan key:generate --show"
```
It will spin up a standalone PHP container and generate the key for you. Expect it to be a 32 character base64 - looks like this: `base64:uFQR0stU/xhOsxwp3RkYDC4FHFsKRu8FlEL0CB9T5Zw=`

## Development Environment
Apart from changing the env in `Docker/.env` you would need to start the app with the dev env compose included like this:
```
docker-compose -f docker-compose.yml -f docker-compose-dev.yml build
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up
```
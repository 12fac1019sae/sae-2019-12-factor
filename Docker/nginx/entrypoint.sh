#!/bin/bash
set -e

envsubst '$NGINX_PORT $PHP_HOSTNAME $PHP_PORT' < /etc/nginx/template/server.template > /etc/nginx/conf.d/server.conf

exec "$@"
#!/bin/bash
set -e

composer install --optimize-autoloader

envsubst '$PHP_PORT' < /usr/local/etc/templates/zzz-app-fpm.template > /usr/local/etc/php-fpm.d/zzz-app-fpm.conf
php-fpm -F -R

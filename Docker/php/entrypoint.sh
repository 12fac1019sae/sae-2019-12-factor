#!/bin/bash
set -e

envsubst '$PHP_PORT' < /usr/local/etc/templates/zzz-app-fpm.template > /usr/local/etc/php-fpm.d/zzz-app-fpm.conf
envsubst '$REDIS_HOST $REDIS_PORT' < /usr/local/etc/php/templates/app-php.template > /usr/local/etc/php/conf.d/app-php.ini

exec "$@"
